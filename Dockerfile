FROM ubuntu:16.04
RUN apt-get -y update && apt-get -y install apache2
RUN echo "<html><br><br><h1><b>TEST DEPLOY BY TERRAFORM+GITLAB+DOCKER</b></h1></html>" > /var/www/html/index.html
CMD ["/usr/sbin/apache2ctl", "-DFOREGROUND"]
EXPOSE 80
